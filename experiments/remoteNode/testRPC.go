package main

import (
	"fmt"
	"log"
	"time"

	rpc "gitlab.com/tzsuite/rpc"
)

func main() {
	url := "https://rpc.smartcontractlabs.ee"
	// url2 := "https://127.0.0.1:8736"
	contract := "tz1gn92H8tWrfhBgYFB8TSLXDqvfntqvo9s7"

	ch := make(chan error)
	shutdown := make(chan struct{})
	r, err := rpc.New(rpc.ConfAlphanet(url), nil, shutdown, ch)
	if err != nil {
		log.Fatalf("creating client: %v", err)
	}

	go func(ch chan error, shutdown chan struct{}) {
		for {
			select {
			case _, more := <-shutdown:
				if !more {
					return
				}
			case err := <-ch:
				fmt.Println("error from goroutine:", err)
				close(shutdown)
			}
		}

	}(ch, shutdown)

	// check that the oracle contract exists
	if err := r.CheckContract(contract); err != nil {
		log.Fatal(fmt.Errorf("checking oracle contract existence: %v", err))
	}

	fmt.Println("It works.")
	close(shutdown)
	time.Sleep(time.Millisecond * 500)
}
