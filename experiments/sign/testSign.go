package main

import (
	"encoding/hex"
	"fmt"
	"log"

	"gitlab.com/tzsuite/tzutil"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/ed25519"
)

func main() {
	fmt.Println("Hello Go!")

	// h := "exprunFkRY8BSAPYkjukmaSm7itmt7PSvuuwvoQu2ZJ9yiE2podp3i"
	// message := "hello"
	h := "expruiCKHbB3AAvczZ18xtjYEMQpgsvUkNwjdjc6HpAww9JhZBJScj"
	seed := "edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh"
	keys, err := tzutil.KeysFromStringSeed(seed)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(keys)

	// tzutil
	sig1 := tzutil.SignData(keys.Secret, []byte(h))
	fmt.Println("sig1:", sig1)

	// zeronet from April to May
	signedBytes2 := ed25519.Sign(keys.Secret, []byte(h))
	signedString2 := hex.EncodeToString(signedBytes2)
	fmt.Println("sig2:", signedString2)

	// alphanet & zeronet from end of May onwards
	signedBytes3 := ed25519.Sign(keys.Secret, blake2b32([]byte(h)))
	signedString3 := hex.EncodeToString(signedBytes3)
	fmt.Println("sig3:", signedString3)

	fmt.Println("h1:", len(h))
	fmt.Println("h2:", len(blake2b32([]byte(h))))
}

func blake2b32(m []byte) []byte {
	hash := blake2b.Sum256(m)
	return hash[:]
}
