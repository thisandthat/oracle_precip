package main

import (
	"crypto/sha256"
	"fmt"

	"github.com/btcsuite/btcutil/base58"
	"golang.org/x/crypto/blake2b"
)

var prefix_tz1 = []byte{6, 161, 159}
var prefix_edpk = []byte{13, 15, 37, 217}

// hashAddress produces a Tezos address (public key hash),
// the procedure is to hash the public key with blake2b, using a Size of 20
// then encode with base58, using a prefix and a checksum
func hashAddress(pubKey []byte) (string, error) {
	b, err := blake2b20(pubKey)
	if err != nil {
		return "", err
	}
	fmt.Println("hashed 20:", b)
	// prefix for operations is [6, 161, 159]
	return CheckEncode(b, prefix_tz1), nil
}

func blake2b20(m []byte) ([]byte, error) {
	h, err := blake2b.New(20, nil)
	if err != nil {
		return nil, fmt.Errorf("creating new hash: %v", err)
	}
	fmt.Println("size:", h.Size())
	fmt.Println("blocksize:", h.BlockSize())
	h.Write(m)
	sum := h.Sum(nil)
	fmt.Println("sum:", sum)
	fmt.Println("length:", len(sum))
	return sum, nil
}

func blake2b32(m []byte) []byte {
	hash := blake2b.Sum256(m)
	return hash[:]
}

// CheckEncode prepends a version byte and appends a four byte checksum.
func CheckEncode(input []byte, version []byte) string {
	b := make([]byte, 0, len(version)+len(input)+4)
	b = append(b, version...)
	b = append(b, input...)
	cksum := checksum(b)
	b = append(b, cksum[:]...)
	return base58.Encode(b)
}

// checksum: first four bytes of sha256^2
func checksum(input []byte) (cksum [4]byte) {
	h := sha256.Sum256(input)
	h2 := sha256.Sum256(h[:])
	copy(cksum[:], h2[:4])
	return
}

// CheckDecode decodes a string that was encoded with CheckEncode and verifies the checksum.
func CheckDecode(input string, prefix []byte) (result []byte, version []byte, err error) {
	decoded := base58.Decode(input)
	if len(decoded) < 4+len(prefix) {
		return nil, nil, base58.ErrInvalidFormat
	}
	version = decoded[:len(prefix)]
	var cksum [4]byte
	copy(cksum[:], decoded[len(decoded)-4:])
	if checksum(decoded[:len(decoded)-4]) != cksum {
		return nil, nil, base58.ErrChecksum
	}
	payload := decoded[len(prefix) : len(decoded)-4]
	result = append(result, payload...)
	return
}

var prefix_edsk_seed = []byte{13, 15, 58, 7}

func main() {
	pubKeyStr := "edpku8vKEfzDSShWqncTEuhhKz63oVRT9ESnjCscjg7YLVbaF6D8fd"
	pubKey, _, err := CheckDecode(pubKeyStr, prefix_edpk)
	if err != nil {
		fmt.Println("err:", err)
		return
	}
	fmt.Println("decoded pubkey:", pubKey)
	pkh, err := hashAddress(pubKey)
	if err != nil {
		fmt.Println("err:", err)
		return
	}
	fmt.Println("pkh:", pkh)
	h, err := blake2b20([]byte(pubKeyStr))
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("hashed string", h)
	fmt.Println("len hashed string", len(h))
	h2 := blake2b32([]byte(pubKeyStr))
	fmt.Println("hashed string 32", h2)
	fmt.Println("len hashed string 32", len(h2))

}
