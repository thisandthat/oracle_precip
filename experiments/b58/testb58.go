package main

import (
	//						    "reflect"
	"crypto/sha256"
	"encoding/hex"
	"fmt"

	"github.com/btcsuite/btcutil/base58"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/ed25519"
)

func sign(k, m []byte) []byte {
	return ed25519.Sign(ed25519.PrivateKey(k), m)
}

// CheckEncode prepends a version byte and appends a four byte checksum.
func CheckEncode(input []byte, version []byte) string {
	b := make([]byte, 0, len(version)+len(input)+4)
	b = append(b, version...)
	b = append(b, input[:]...)
	cksum := checksum(b)
	b = append(b, cksum[:]...)
	return base58.Encode(b)
}

// CheckDecode decodes a string that was encoded with CheckEncode and verifies the checksum.
func CheckDecode(input string) (result []byte, version []byte, err error) {
	decoded := base58.Decode(input)
	if len(decoded) < 5 {
		return nil, nil, base58.ErrInvalidFormat
	}
	version = decoded[:4]
	var cksum [4]byte
	copy(cksum[:], decoded[len(decoded)-4:])
	if checksum(decoded[:len(decoded)-4]) != cksum {
		return nil, nil, base58.ErrChecksum
	}
	payload := decoded[4 : len(decoded)-4]
	result = append(result, payload...)
	return
}

// checksum: first four bytes of sha256^2
func checksum(input []byte) (cksum [4]byte) {
	h := sha256.Sum256(input)
	h2 := sha256.Sum256(h[:])
	copy(cksum[:], h2[:4])
	return
}

func main() {
	fmt.Println("Hello, playground")
	str := "00d259fab932afb760e21092fa8c10cdedf363aa908e7200b4530a847354f35400020000cb7f83bcd39b079ed9c64149dc1cdc1090da4247000000000000c3500000003b0000006600004507df03715c9c53d517ec10a9165353339f386487df7f1796b7787e3cf3a4a001000000000004ce780000028fbe0ae37ad0ed587e85640fabdb04b64c0d5a01030c01000000000004d2600000b31d63b6e0755b6d52de3908aa45699a0408f01501030c"
	b, err := hex.DecodeString(str)
	if err != nil {
		fmt.Println(err)
		return
	}
	hash := blake2b.Sum256(b)
	str2 := hex.EncodeToString(hash[:])
	fmt.Println("blake2b:", str2)
	//	prefix := []byte{5, 116}
	prefix1 := byte(5)
	prefix2 := []byte{116}
	//	fmt.Println("prefix:", prefix)
	cat := append(prefix2, hash[:]...)
	fmt.Println("cat:", cat)
	str3 := base58.CheckEncode(cat, prefix1)
	fmt.Println("b58:", str3)
	str5 := CheckEncode(hash[:], []byte{5, 116})
	fmt.Println("encodev2:", str5)
	if str5 == str3 {
		fmt.Println("encodev2 is gucci")
	}
}
