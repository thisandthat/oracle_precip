package main

import (
	"encoding/hex"
	"fmt"

	"golang.org/x/crypto/blake2b"
)

func main() {
	fmt.Println("Hello, playground")
	str := "0046f13eca313978b422f2edf13b2a4e71fbd8ec25f85b6e71b6beadfb5ead4600020000cb7f83bcd39b079ed9c64149dc1cdc1090da424700000000000000010000002b000000200100000000075bcd150000028fbe0ae37ad0ed587e85640fabdb04b64c0d5a00"
	b, err := hex.DecodeString(str)
	if err != nil {
		fmt.Println(err)
		return
	}
	hash := blake2b.Sum256(b)
	str2 := hex.EncodeToString(hash[:])
	fmt.Println(str2)
}
