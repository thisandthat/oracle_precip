package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/blendle/zapdriver"
	"gitlab.com/thisandthat/oracle_precip/internal/storage"
)

const input = `{ "prim": "Pair",
"args":
	[ { "string": "tz1RaCALFxA4JG9St6JhjSoAxtYrB58hbwad" },
		[ { "prim": "Elt",
				"args":
					[ { "bytes":
								"ed96fa23b0d4c655eb80f6c48908b7de7ac8f26f0539f998b4cc5aaa35ec36e8" },
						{ "prim": "Pair",
							"args":
								[ { "prim": "Pair",
										"args":
											[ { "bytes": "deadbeef1234" },
												{ "string":
														"KT1S84MKtNjiJKmpaiKMsmYjwowheNujTic9" } ] },
									{ "prim": "Pair",
										"args":
											[ { "prim": "Pair",
													"args":
														[ { "int": "123456" }, { "int": "456789" } ] },
												{ "string": "2018-12-14T14:57:00Z" } ] } ] } ] } ] ] }`

func main() {
	logger, err := zapdriver.NewDevelopment()
	if err != nil {
		fmt.Fprintf(os.Stderr, "setting up zap: %v\n", err)
		os.Exit(1)
	}
	defer logger.Sync() // flushes buffer, if any
	log := logger.Sugar()

	jobs, err := storage.ExtractJobs(log, json.RawMessage(input))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%#v\n", jobs)

}
