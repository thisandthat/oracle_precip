package types

import "github.com/shawntoffel/darksky"

type Job struct {
	Hash    string
	Nonce   string
	Address string
	darksky.ForecastRequest
}
