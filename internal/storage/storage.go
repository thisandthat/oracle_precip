package storage

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/shawntoffel/darksky"

	"github.com/savaki/jq"
	"gitlab.com/thisandthat/oracle_precip/internal/types"
	"gitlab.com/tzsuite/tzutil"
	"go.uber.org/zap"
)

// storage
//   (pair (address :oracle_pkh)
//         (map bytes
//              (pair (pair (bytes :nonce) address)
// 				      (pair :params (pair :location int int)
//                          timestamp))));

var parseRequests jq.Op
var parseRequestHash jq.Op
var parseRequestNonce jq.Op
var parseRequestAddress jq.Op
var parseRequestLatitude jq.Op
var parseRequestLongitude jq.Op
var parseRequestTime jq.Op

func init() {
	parseRequests = jq.Must(jq.Parse(".args.[1]"))
	parseRequestHash = jq.Must(jq.Parse(".args.[0].bytes"))
	parseRequestNonce = jq.Must(jq.Parse(".args.[1].args.[0].args.[0].bytes"))
	parseRequestAddress = jq.Must(jq.Parse(".args.[1].args.[0].args.[1].string"))
	parseRequestLatitude = jq.Must(jq.Parse(".args.[1].args.[1].args.[0].args.[0].int"))
	parseRequestLongitude = jq.Must(jq.Parse(".args.[1].args.[1].args.[0].args.[1].int"))
	parseRequestTime = jq.Must(jq.Parse(".args.[1].args.[1].args.[1].string"))
}

// ExtractJobs takes JSON of michelson contract information, and outputs an array with details of requested jobs.
func ExtractJobs(log *zap.SugaredLogger, storage json.RawMessage) ([]types.Job, error) {
	rawElems, err := parseRequests.Apply(storage)
	if err != nil {
		return nil, fmt.Errorf("parsing requests: %v", err)
	}
	log.Debug("Requests:", string(rawElems))

	var elems []json.RawMessage
	err = json.Unmarshal(rawElems, &elems)
	if err != nil {
		return nil, fmt.Errorf("unmarshaling requests: %v", err)
	}

	var jobs []types.Job
	for _, e := range elems {
		v, err := parseRequestHash.Apply(e)
		if err != nil {
			return nil, fmt.Errorf("parsing request nonce: %v", err)
		}
		hash := strings.TrimFunc(string(v), func(r rune) bool { return r == '"' })
		log.Debug("Request hash:", hash)

		v, err = parseRequestNonce.Apply(e)
		if err != nil {
			return nil, fmt.Errorf("parsing request nonce: %v", err)
		}
		nonce := strings.TrimFunc(string(v), func(r rune) bool { return r == '"' })
		log.Debug("Request nonce:", nonce)

		v, err = parseRequestAddress.Apply(e)
		if err != nil {
			return nil, fmt.Errorf("parsing request nonce: %v", err)
		}
		address := strings.TrimFunc(string(v), func(r rune) bool { return r == '"' })
		log.Debug("Request address:", address)

		v, err = parseRequestLatitude.Apply(e)
		if err != nil {
			return nil, fmt.Errorf("parsing request nonce: %v", err)
		}
		lat, err := parseMeasurement(v)
		if err != nil {
			return nil, fmt.Errorf("parsing darksky measurement: %v", err)
		}
		log.Debug("Request latitude:", lat)

		v, err = parseRequestLongitude.Apply(e)
		if err != nil {
			return nil, fmt.Errorf("parsing request nonce: %v", err)
		}
		lon, err := parseMeasurement(v)
		if err != nil {
			return nil, fmt.Errorf("parsing darksky measurement: %v", err)
		}
		log.Debug("Request lon:", lon)

		v, err = parseRequestTime.Apply(e)
		if err != nil {
			return nil, fmt.Errorf("parsing request time: %v", err)
		}
		t, err := tzutil.ParseTimestamp(v)
		if err != nil {
			return nil, fmt.Errorf("parsing Tezos timestamp: %v", err)
		}
		log.Debug("Request time:", t)

		jobs = append(jobs, types.Job{
			Hash:    hash,
			Nonce:   nonce,
			Address: address,
			ForecastRequest: darksky.ForecastRequest{
				Latitude:  lat,
				Longitude: lon,
				Time:      darksky.Timestamp(t.Unix()),
			},
		})
	}

	return jobs, nil
}

func parseMeasurement(value []byte) (darksky.Measurement, error) {
	str := strings.TrimFunc(string(value), func(r rune) bool { return r == '"' })
	i, err := strconv.Atoi(str)
	if err != nil {
		return 0, err
	}
	lat := darksky.Measurement(float64(i) / 10000)
	return lat, nil
}
