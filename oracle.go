package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/thisandthat/oracle_precip/internal/storage"
	"gitlab.com/thisandthat/oracle_precip/internal/types"
	"gitlab.com/tzsuite/rpc"
	"gitlab.com/tzsuite/tzutil"
	"go.uber.org/zap"
	"google.golang.org/appengine"

	"github.com/blendle/zapdriver"
	"github.com/shawntoffel/darksky"
)

var seed = flag.String("seed", "", "specify a seed from which to generate the keys to sign data with")
var contract = flag.String("contract", "", "specify an address of the oracle contract")

var rpcURL = flag.String("rpcurl", "", "specify the base url of the tezos-node RPC")

var debug = flag.Bool("debug", false, "print debug information")
var log *zap.SugaredLogger

var apiKey = flag.String("apikey", "", "specify a key for Dark Sky API")

// const apiKey = "0123456789abcdef9876543210fedcba"

const fee int64 = 50000

func cloudConf() {
	if appengine.IsAppEngine() {
		if env := os.Getenv("SEED"); env != "" {
			*seed = env
		}
		if env := os.Getenv("CONTRACT"); env != "" {
			*contract = env
		}
		if env := os.Getenv("RPCURL"); env != "" {
			*rpcURL = env
		}
		if env := os.Getenv("APIKEY"); env != "" {
			*apiKey = env
		}
		if env := os.Getenv("DEBUG"); env != "" {
			*debug = true
		}
	}
}

func main() {
	flag.Parse()
	cloudConf()

	loggerFunc := zapdriver.NewProduction
	if *debug {
		loggerFunc = zapdriver.NewDevelopment
	}
	logger, err := loggerFunc()
	if err != nil {
		fmt.Fprintf(os.Stderr, "setting up zap: %v\n", err)
		os.Exit(1)
	}
	defer logger.Sync() // flushes buffer, if any
	log = logger.Sugar()

	log.Debug("seed:", *seed)
	log.Debug("contract:", *contract)
	log.Debug("API key:", *apiKey)

	client := darksky.New(*apiKey)

	// regenerate keys from seed
	keys, err := tzutil.KeysFromStringSeed(*seed)
	if err != nil {
		log.Fatal(fmt.Errorf("keys: %v", err))
	}

	log.Debugf("keys: %#v", keys)

	ch := make(chan error)
	shutdown := make(chan struct{})
	newBlock := make(chan string)
	r, err := rpc.New(ConfAlphanet(*rpcURL, log), newBlock, shutdown, ch)
	if err != nil {
		log.Fatalf("creating client: %v", err)
	}

	// check that the oracle contract exists
	if err := r.CheckContract(*contract); err != nil {
		if rpc.IsNotFound(err) {
			log.Warn("The contract doesn't seem to exist on the network. Make sure the contract is originated, or correct the address.")
		}
		log.Fatal(fmt.Errorf("checking oracle contract existence: %v", err))
	}

	if appengine.IsAppEngine() {
		http.HandleFunc("/_ah/start", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
		})
		go log.Fatal(http.ListenAndServe(":8080", nil))
	}

	go dispatchAnswers(client, r, keys, shutdown, newBlock, ch)
	for {
		select {
		case err := <-ch:
			close(shutdown)
			log.Fatal(fmt.Errorf("error from a goroutine: %v", err))
		}
	}
}

// getJobs checks contract storage and parses out the new requests for data
func getJobs(rpcClient *rpc.RPC, contract string) ([]types.Job, error) {
	cs, err := rpcClient.Contract(contract)
	if err != nil {
		return nil, err
	}
	jobs, err := storage.ExtractJobs(log, cs.Script.Storage)
	if err != nil {
		return nil, err
	}
	return jobs, nil
}

// answer gets a Job, gets data from the web API and answers the query request by transferring the data to the caller
func answer(client darksky.DarkSky, rpcClient *rpc.RPC, keys tzutil.Keys, jobs []types.Job, shutdown <-chan struct{}) error {
	for _, j := range jobs {
		prob, err := getProbability(client, j.ForecastRequest)
		if err != nil {
			return fmt.Errorf("getting data: %v", err)
		}
		done, failed, err := rpcClient.BatchTransfer(keys, 0, keys.PKH, j.Address, prepareAnswer(j.Hash, j.Nonce, prob))
		if err != nil {
			return fmt.Errorf("transferring data: %v", err)
		}
		log.Info("transfer queued for next batch")
		go reportOp(
			fmt.Sprintf("transfer of data %v to %s", string(prepareAnswer(j.Hash, j.Nonce, prob)), j.Address),
			fmt.Sprintf("transfer of data %v to %s", string(prepareAnswer(j.Hash, j.Nonce, prob)), j.Address),
			done,
			failed,
			shutdown)
		done, failed, err = rpcClient.BatchTransfer(keys, 0, keys.PKH, *contract, prepareDelete(j.Hash))
		if err != nil {
			return fmt.Errorf("deleting query: %v", err)
		}
		log.Info("deletion queued for next batch")
		go reportOp(
			fmt.Sprintf("deletion of request %s", j.Hash),
			fmt.Sprintf("deletion of request %s", j.Hash),
			done,
			failed,
			shutdown)
	}
	return nil
}

func reportOp(injectedMessage string, includedMessage string, done <-chan string, failed <-chan error, shutdown <-chan struct{}) {
	select {
	case opHash := <-done:
		log.Infof("%s injected in operation: %s", injectedMessage, opHash)
	case err := <-failed:
		log.Infof("%s failed: %s", injectedMessage, err)
		return
	case _, more := <-shutdown:
		if !more {
			return
		}
	}
	select {
	case blockHash := <-done:
		log.Infof("%s included in block: %s", includedMessage, blockHash)
	case err := <-failed:
		log.Infof("%s failed: %s", includedMessage, err)
		return
	case _, more := <-shutdown:
		if !more {
			return
		}
	}
}

func getProbability(client darksky.DarkSky, request darksky.ForecastRequest) (int, error) {
	// use metric units
	request.Options.Units = "si"
	resp, err := client.Forecast(request)
	if err != nil {
		return 0, err
	}
	hourly := resp.Hourly.Data
	var d darksky.DataPoint
	for _, h := range hourly {
		if request.Time < h.Time {
			break
		}
		d = h
	}
	prob := int(round(float64(d.PrecipProbability) * 100))
	log.Debug("probability from darksky API:", d.PrecipProbability, prob)
	return prob, nil
}

func dispatchAnswers(client darksky.DarkSky, rpcClient *rpc.RPC, keys tzutil.Keys, shutdown <-chan struct{}, newBlock <-chan string, ch chan<- error) {
	for {
		select {
		case _, more := <-shutdown:
			if !more {
				return
			}
		case _ = <-newBlock:
			log.Info("dispatching a batch")
			jobs, err := getJobs(rpcClient, *contract)
			if err != nil {
				ch <- fmt.Errorf("getting jobs: %v", err)
			}
			err = answer(client, rpcClient, keys, jobs, shutdown)
			if err != nil {
				ch <- fmt.Errorf("answering jobs: %v", err)
			}
		}
	}
}
