package main

import (
	"encoding/json"
	"fmt"
)

func prepareAnswer(requestID string, nonce string, data int) json.RawMessage {
	return json.RawMessage(fmt.Sprintf(`{ "prim": "Pair",
	"args":
	  [ { "bytes": "%s" },
		{ "prim": "Pair",
		  "args": [ { "bytes": "%s" }, { "int": "%d" } ] } ] }`,
		requestID, nonce, data))
}

func prepareDelete(requestID string) json.RawMessage {
	return json.RawMessage(fmt.Sprintf(`{"prim":"Left","args":[{"bytes":"%s"}]}`, requestID))
}
